#!/bin/bash
BATTERY=`acpi 2>/dev/null| cut -d "," -f 2 | xargs | cut -d "%" -f 1`
if [ "$BATTERY" == "" ]; then
	exit;
fi
if [ "$BATTERY" -lt 95 ]; then
	echo "◎ $BATTERY%"
fi
