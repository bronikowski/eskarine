#!/bin/bash

# xwinwrap -b -fs -sp -nf -ov -- mplayer -wid WID -quiet -loop -loop $1
if [ "$1" == "" ]; then
	file="$HOME/.eskarine/Wallpapers/animated/`ls ~/.eskarine/Wallpapers/animated/ | shuf -n1`"
else
	file=$1
fi
xwinwrap -b -fs -sp -nf -ov -- mplayer -wid WID -quiet -loop -loop $file
