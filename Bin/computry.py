#! env python3
import requests
import sys
try:
    computry = requests.get('https://fuse.pl/state/computry').json()['data']
except KeyError:
    request.get('https://fuse.pl/state')
    sys.exit(1)
parsed = dict()
for computer in computry.keys():
    hostname, data_type = computer.split('_')
    try:
        parsed[hostname]
    except KeyError:
        parsed[hostname] = dict()
    parsed[hostname][data_type] = computry[computer]

for p in parsed.keys():
    print("≈ {hostname}({ips}) ⏳: {time}".format(
        hostname=p,
        ips=parsed[p]['ips'].replace(' ', ', '),
        time=parsed[p]['seen'].replace(',',', ')
    ))
