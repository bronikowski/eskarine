#!/bin/bash

vagrant status | grep "The VM is running" > /dev/null
if [ $? -eq 0 ]; then
	echo -e "\e[1mYour virtual machine is running."
	echo "Virtual machine have to halted to perform this script."
	exit
fi
vagrant snapshot list | grep "No snapshots have been taken yet" > /dev/null
if [ $? -eq 0 ]; then
	echo "There are no snapshot taken"
	exit
fi
for s in `vagrant snapshot list`; do
	echo -e "Removing \e[4m$s\e[0m"
	vagrant snapshot delete "$s"
done
