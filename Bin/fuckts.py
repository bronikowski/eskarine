#!/usr/bin/env python3
import wikipedia
import sqlite3
import os
import sys
import textwrap
import progressbar

CREATE_TABLE = """CREATE table fuckts(id INTEGER PRIMARY KEY, title TEXT, url TEXT, body TEXT, seen INT DEFAULT 0)"""
GET_RANDOM_PAGE = """SELECT title, body, url, id FROM fuckts WHERE seen = 0 ORDER BY id LIMIT 1"""
MARK_AS_SEEN = """UPDATE fuckts SET seen = 1 WHERE id = ?"""
STILL_TO_COME = """SELECT COUNT(1) FROM fuckts WHERE seen = 0"""
ADD_PAGE = """INSERT INTO fuckts(title, body, url, seen) VALUES (?,?,?,0)"""
DELETE_OLD = """DELETE FROM fuckts WHERE seen = 1"""

force_refresh = False
delete_old = False
pages_to_cache = 20

if len(sys.argv) == 2:
    if sys.argv[1] == "--force-refresh":
        force_refresh = True
if len(sys.argv) == 3:
    if sys.argv[1] == "--force-refresh":
        pages_to_cache = int(sys.argv[2])
        force_refresh = True
    if sys.argv[1] == "--purge":
        delete_old = True
try:
    cache_folder = os.environ['XDG_CACHE_HOME']
except KeyError:
    cache_folder = os.path.join(os.environ['HOME'], '.cache')

try:
    TARGET=os.path.join(cache_folder, '.fuckts.db')
except IndexError:
    sys.exit(1)
def refresh_cache(connection, pages_to_cache=20):
    cursor = connection.cursor()
    bar = progressbar.ProgressBar(max_value=pages_to_cache)
    print("Commence refreshing {}".format(pages_to_cache))
    for i in range(pages_to_cache):
        try:
            title = wikipedia.random()
            page = wikipedia.page(title)
            cursor.execute(ADD_PAGE, (title, page.summary, page.url))
        except wikipedia.exceptions.DisambiguationError:
            # nothing to do here, not worth to fight it
            pass
        except wikipedia.exceptions.PageError:
            print("Page {} can't be loaded".format(title))
        bar.update(i)
    connection.commit()

def render(title, body, url):
    print("{title} ({url})\n\n{body}\n".format(title=title, body="\n".join(textwrap.wrap(body)), url=url))

if not os.path.exists(TARGET):
    connection = sqlite3.connect(TARGET)
    cursor = connection.cursor()
    cursor.execute(CREATE_TABLE)
else:
    connection = sqlite3.connect(TARGET)
    cursor = connection.cursor()
    cursor.execute(GET_RANDOM_PAGE)
    exists = cursor.fetchone()
    if exists:
        title, body, url, obj_id = exists

        try:
            cursor.execute(MARK_AS_SEEN, (obj_id,))
            render(title, body, url)
        except sqlite3.OperationalError:
            print("Sorry Dave, I can not let you do this…")
            sys.exit()
    if delete_old:
        cursor.execute(DELETE_OLD)
        connection.commit()
        

unread = cursor.execute(STILL_TO_COME).fetchone()[0]
if(unread < 5 or force_refresh):
    try:
        refresh_cache(connection, pages_to_cache)
    except KeyboardInterrupt:
        # ^c, commit and exit with status error 1
        connection.commit()
        sys.exit(1)
connection.commit()
