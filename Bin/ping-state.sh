#!/bin/bash

curl -s "https://fuse.pl/state/computry/`hostname`_ips/`hostname --all-ip-addresses`" >/dev/null
curl -s "https://fuse.pl/state/computry/`hostname`_seen/`date +%Y-%m-%d,%H:%M`" > /dev/null
