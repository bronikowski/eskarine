#!/bin/bash

if [ -f /tmp/queue.txt ] ; then
	COUNTER=`cat /tmp/queue.txt | tr -d '\n'`
	if [ "$COUNTER" != "0" ]; then
		echo -n "☄ $COUNTER";
	fi
else
	echo -n ":("
fi
