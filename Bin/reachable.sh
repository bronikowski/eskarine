#!/bin/bash

ADDRESS=${1-https://fuse.pl}
DET=${2-☠}

curl -I -s $ADDRESS > /dev/null
if [ $? -gt 0 ]; then
	echo $DET
fi
