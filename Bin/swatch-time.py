#!/usr/bin/env python3
from datetime import datetime, timedelta
import pytz
import sys

def swatch_time(datetime_object = None, timezone_literal = 'Europe/Warsaw'):

    reset_clock = {
            'hour': 0,
            'minute': 0,
            'second': 0,
            'microsecond': 0
    }

    if not datetime_object:
        datetime_object = datetime.now()

    if not datetime_object.tzinfo:
        timezone = pytz.timezone(timezone_literal)
        datetime_object = timezone.localize(datetime_object)

    # Biel TZ is a unrecognized TZ that can be expressed as UTC+1
    # Thanks, Swatch
    swatch_timezone = pytz.timezone('UTC')
    datetime_object = datetime_object.astimezone(swatch_timezone) + timedelta(hours=1)

    midnight = datetime_object.replace(**reset_clock)
    time_delta = datetime_object - midnight

    if time_delta.seconds == 0:
        # don't divide against zero
        return 0

    return int(time_delta.seconds / 86.4)

if __name__ == "__main__":
    print("@{}".format(swatch_time()))
