#! /bin/bash

TMPFOX=$(mktemp -d)
echo "Creating empty profile $TMPFOX"
echo "Starting virgin Firefox"
~/firefox/firefox -profile $TMPFOX -no-remote -new-instance
rm -rf $TMPFOX
echo "Cleanup"
