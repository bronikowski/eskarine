#!/bin/bash

ffmpeg -i "$1" -c:v libx264 -crf 18 -vf format=yuv420p -c:a copy "$2"
