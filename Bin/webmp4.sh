#!/bin/bash

if [ "$1" == "" ]; then
				echo "Pass input file"
				exit
fi

OUTPUT=${2:-output.mp4}
ffmpeg -i "$1" -vcodec h264 -acodec aac -strict -2 $OUTPUT
