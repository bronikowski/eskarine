
#

sudo apt install -y mercurial freeglut3-dev libgl1-mesa-dev cmake

cd ~/src
if [ -d ~/src/bearlibterminal ]; then
	cd ~/src/bearlibterminal
	hg update --clean
	hg pull
else
	hg clone https://bitbucket.org/cfyzium/bearlibterminal
	cd bearlibterminal
fi

cmake CMakeLists.txt
make
sudo cp Output/Linux64/libBearLibTerminal.so /usr/lib/libbearlibterminal.so
sudo ln -sf /usr/lib/libbearlibterminal.so /usr/lib/libBearLibTerminal.so
sudo cp Terminal/Include/C/BearLibTerminal.h /usr/local/include
