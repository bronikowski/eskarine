#!/bin/bash

cd ~/src
wget http://repo1.maven.org/maven2/org/clojure/clojure/1.8.0/clojure-1.8.0.zip
unzip clojure-1.8.0.zip
wget https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein -O lein
chmod u+x lein
./lein
