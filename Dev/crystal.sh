if [[ $EUID -ne 0 ]]; then
	echo "Be root"
	exit
fi
apt-key adv --keyserver keys.gnupg.net --recv-keys 09617FD37CC06B54
echo "deb http://dist.crystal-lang.org/apt crystal main" > /etc/apt/sources.list.d/crystal.list
apt update
apt install crystal
