cd ~/src

if [ -d darling ]; then
	echo "Ready"
else
	git clone --recursive https://github.com/darlinghq/darling.git
	sudo apt install cmake clang-4.0 bison flex xz-utils libfuse-dev libudev-dev pkg-config libc6-dev-i386 linux-headers-amd64 libcap2-bin git libcairo2-dev libgl1-mesa-dev libtiff5-dev libfreetype6-dev libxml2-dev libegl1-mesa-dev libfontconfig1-dev libbsd-dev
fi


cd darling

git reset --hard
git pull

mkdir -p build
cd build
cmake ..
make 
make lkm
sudo make lkm_install
sudo make install
