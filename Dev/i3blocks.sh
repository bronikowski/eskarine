#!/bin/bash

cd ~/src
if [ -d ~/src/i3blocks ];  then
	cd ~/src/i3blocks
	git reset --hard
	git pull
else
	git clone https://github.com/vivien/i3blocks.git
	cd i3blocks
	./autogen.sh
fi

./configure
sudo make install
