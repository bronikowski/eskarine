#!/bin/bash

if [ -d ~/src/indicator-kdeconnect ]; then
	cd ~/src/indicator-kdeconnect
	git reset --hard
else
	cd ~/src
	git clone https://github.com/Bajoja/indicator-kdeconnect.git
	cd ~/src/indicator-kdeconnect
fi
sudo apt install libappindicator3-dev kdeconnect libspice-client-gtk-3.0-dev valac
cmake .
make
sudo make install
