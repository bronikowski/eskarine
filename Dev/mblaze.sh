#!/bin/bash

cd ~/src
if [ -d ~/src/mblaze ] ; then
	cd ~/src/mblaze
	git reset --hard
	git pull
else
	cd ~/src/mblaze
	git clone https://github.com/chneukirchen/mblaze.git
fi

cd ~/src/mblaze
sudo make install
