cd ~/src

if [ -d musikcube ]; then
	cd musikcube
	git reset --hard
	git pull
else
	git clone https://github.com/clangen/musikcube.git
	cd musikcube
fi
sudo apt install build-essential clang cmake libboost-thread1.55-dev libboost-system1.55-dev libboost-filesystem1.55-dev libboost-date-time1.55-dev libboost-atomic1.55-dev libboost-chrono1.55-dev libogg-dev libvorbis-dev libflac-dev libfaad-dev libncursesw5-dev libasound2-dev libpulse-dev pulseaudio libmicrohttpd-dev libmp3lame-dev libcurl4-openssl-dev libtag1-dev
cmake .
sudo make install
