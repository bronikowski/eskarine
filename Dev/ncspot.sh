#!/bin/bash

cd ~/src
if [ -d ncspot ]; then
	cd ncspot
	git reset --hard
	git pull
else
	git clone https://github.com/hrkfdn/ncspot.git
fi

cargo build --release
sudo cp target/release/ncspot /usr/local/bin
