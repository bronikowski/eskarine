#!/bin/bash

cd ~/src
if [ -d ~/src/Nim ]; then
	cd Nim
	git reset --hard
	git pull
else
	git clone -b master git://github.com/nim-lang/Nim.git
	cd Nim
	git clone -b master --depth 1 git://github.com/nim-lang/csources
fi
cd csources && sh build.sh
cd ..
bin/nim c koch
./koch boot -d:release
./koch nimble
./koch tools
