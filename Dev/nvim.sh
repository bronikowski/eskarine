#!/bin/bash

cd ~/src
mkdir -p ~/.config/nvim
ln -fs ~/.vimrc ~/.config/nvim/init.vim
if [ -d ~/src/neovim ]; then
	cd neovim
	git reset --hard
	git pull
else
	git clone https://github.com/neovim/neovim.git
	cd neovim
	git reset --hard
	git pull
fi

sudo rm -rf .deps
sudo rm -rf build
sudo make install
sudo make clean
