#!/bin/bash

sudo apt -y install cvs 

if [ -d ~/src/libowfat ]; then
	cd ~/src/libowfat
	cvs update -P -d
	make
else
	cd ~/src
	cvs -d :pserver:cvs@cvs.fefe.de:/cvs -z9 co libowfat
	cd libowfat
	make
fi

if [ -d ~/src/opentracker ]; then
	cd ~/src/opentracker
	git reset --hard
	git pull
	make
else
	git clone git://erdgeist.org/opentracker ~/src/opentracker
	cd ~/src/opentracker
	make
fi
