#!/bin/bash

cd ~/src
if [ -d ~/src/cpython ];  then
	cd ~/src/cpython
	git reset --hard
	git pull
else
	git clone https://github.com/python/cpython.git
	cd cpython
fi
./configure --enable-optimizations
sudo make install
sudo rm /usr/local/bin/python3 # remove pesky symlink
sudo make clean
