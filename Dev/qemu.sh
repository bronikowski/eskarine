#!/bin/bash

cd ~/src
sudo apt-get -y install git libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev
sudo apt-get -y install git-email
sudo apt-get -y install libaio-dev libbluetooth-dev libbrlapi-dev libbz2-dev
sudo apt-get -y install libcap-dev libcap-ng-dev libcurl4-gnutls-dev libgtk-3-dev
sudo apt-get -y install libibverbs-dev libjpeg8-dev libncurses5-dev libnuma-dev
sudo apt-get -y install librbd-dev librdmacm-dev
sudo apt-get -y install libsasl2-dev libsdl1.2-dev libseccomp-dev libsnappy-dev libssh2-1-dev
sudo apt-get -y install libvde-dev libvdeplug-dev libvte-2.91-dev libxen-dev liblzo2-dev
sudo apt-get -y install valgrind xfslibs-dev 
sudo apt-get -y install libnfs-dev libiscsi-dev

if [ -d ~/src/qemu ];  then
	cd ~/src/qemu
	git reset --hard
	git pull
else
	git clone git://git.qemu-project.org/qemu.git
	cd qemu
fi
./configure --target-list="i386-softmmu x86_64-softmmu arm-softmmu"
sudo make install
