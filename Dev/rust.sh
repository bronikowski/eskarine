#!/bin/bash

if [ -d ~/.cargo/ ]; then
	source ~/.cargo/env
	rustup self update
	rustup update
	cargo install-update rustfmt
else
	curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain stable
	source ~/.cargo/env
	sudo apt install gcc-mingw-w64-x86-64 -y
	sudo apt-get install -qq gcc-arm-linux-gnueabihf
	rustup target add x86_64-pc-windows-gnu
	rustup target add armv7-unknown-linux-gnueabihf
	cp Dots/cargo ~/.cargo/config
	cargo install cargo-update
	cargo install rustfmt
fi
