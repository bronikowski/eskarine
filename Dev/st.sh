cd ~/src

if [ -d st ]; then
	# Ready
	echo "Ready"
else
	git clone git://git.suckless.org/st
	sudo apt install libxft-dev libx11-dev libfreetype6-dev
fi

cd st
git reset --hard
git pull origin master
make
