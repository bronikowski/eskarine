cd ~/src

if [ -d ttyd ]; then
	echo "Ready"
else
	sudo apt-get install cmake g++ pkg-config git vim-common libwebsockets-dev libjson-c-dev libssl-dev
	git clone https://github.com/tsl0922/ttyd.git
fi


cd ttyd
if [ -d build ]; then
	rm -rf build
fi

git reset --hard
git pull origin master

mkdir build
cd build
cmake ..
make 
sudo make install
