#!/bin/bash

cd ~/src
if [ -d ~/src/webdis ];  then
	cd ~/src/webdis
	git reset --hard
	git pull
else
	sudo apt install redis-server redis-tools libevent-dev
	git clone https://github.com/nicolasff/webdis.git
	cd webdis
fi

sudo make install
