cd ~/src

if [ -d wrk ]; then
	cd wrk
	git reset --hard
	git pull
else
	git clone https://github.com/wg/wrk.git
	cd wrk
fi

make
sudo cp wrk /usr/local/bin
