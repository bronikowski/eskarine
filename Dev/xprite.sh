#!/bin/bash

cd ~/src
if [ -d xprite-editor ]; then
	cd xprite-editor
	git reset --hard
	git submodule foreach git pull origin master
else
	git clone https://github.com/rickyhan/xprite-editor --recurse-submodules
	cd xprite-editor
fi

cargo build --bin xprite-native --release
