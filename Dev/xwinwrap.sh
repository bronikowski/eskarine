cd ~/src

if [ -d xwinwrap ]; then
	# Ready
	echo "Ready"
else
	sudo apt install libxext-dev
	git clone https://github.com/lrewega/xwinwrap.git
fi

cd xwinwrap
git reset --hard
git pull origin master
make
sudo make install
