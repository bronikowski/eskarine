hdr_order from: reply-to: to: cc: subject: date:
ignore *
unignore from: reply-to: to: cc: subject: date:

set query_command="goobook query '%s'"
bind editor <Tab> complete-query
bind attach <return> view-mailcap
bind index,pager G group-reply
bind index,pager x noop
macro index,pager gf  '<enter-command>unset wait_key<enter><shell-escape>imapfilter<enter>'
macro index,pager gg  '<enter-command>unset wait_key<enter><shell-escape>offlineimap -o; notmuch new<enter>'
macro index,pager gs  '<enter-command>unset wait_key<enter><shell-escape>r2e.sh run<enter>'
macro index,pager gi '<change-folder>=INBOX<enter>'
macro index,pager @ '<tag-prefix-cond><save-message>~/Maildir/fuse.pl/INBOX.TODO<enter>$' 
macro index,pager gb '<tag-prefix-cond><save-message>~/Maildir/fuse.pl/INBOX<enter>$' 
macro index A "<tag-pattern>~N<enter><tag-prefix><clear-flag>N<untag-pattern>.<enter>" "mark all new as read"

set spoolfile="~/Maildir/fuse.pl/INBOX"
set folder="~/Maildir/fuse.pl/"
set imap_check_subscribed

set from = "emil@fuse.pl"
set realname = "Emil Oppeln-Bronikowski"

# For better looks
set markers=no # don't put '+' at the beginning of wrapped lines
set pager_index_lines= 5 # how large is the index window?
set sort = 'threads'
set sort_aux = 'last-date-received'

# My Editor
#set editor                = 'nano -m -S --syntax=/usr/share/nano/mutt.nanorc'
set editor = 'vim -c ProseMode'

unset mark_old
unset markers
unset move
unset sig_dashes
set header_cache = "~/.mutt/headers/"
set message_cachedir = "~/.mutt/body/"
set signature="~/.signature"
#set signature="~/.eskarine/Bin/fuckts.py|"

set pgp_autosign=no
set pgp_sign_as=0x96769184
set pgp_replyencrypt=yes
set pgp_timeout=3660
set crypt_replyencrypt=yes
set crypt_opportunistic_encrypt


set pgp_decode_command="gpg --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet --batch --output - %f"

# verify a pgp/mime signature
set pgp_verify_command="gpg --status-fd=2 --no-verbose --quiet --batch --output - --verify %s %f"

# decrypt a pgp/mime attachment
set pgp_decrypt_command="gpg --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet --batch --output - %f"

# create a pgp/mime signed attachment
# set pgp_sign_command="gpg-2comp --comment '' --no-verbose --batch --output - %?p?--passphrase-fd 0? --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_sign_command="gpg --no-verbose --batch --quiet --output - %?p?--passphrase-fd 0? --armor --detach-sign --textmode %?a?-u %a? %f"

# create a application/pgp signed (old-style) message
# set pgp_clearsign_command="gpg-2comp --comment '' --no-verbose --batch --output - %?p?--passphrase-fd 0? --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_clearsign_command="gpg --no-verbose --batch --quiet --output - %?p?--passphrase-fd 0? --armor --textmode --clearsign %?a?-u %a? %f"

# create a pgp/mime encrypted attachment
# set pgp_encrypt_only_command="pgpewrap gpg-2comp -v --batch --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg --batch --quiet --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"

# create a pgp/mime encrypted and signed attachment
# set pgp_encrypt_sign_command="pgpewrap gpg-2comp %?p?--passphrase-fd 0? -v --batch --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap gpg %?p?--passphrase-fd 0? --batch --quiet --no-verbose --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"

# import a key into the public key ring
set pgp_import_command="gpg --no-verbose --import %f"

# export a key from the public key ring
set pgp_export_command="gpg --no-verbose --export --armor %r"

# verify a key
set pgp_verify_key_command="gpg --verbose --batch --fingerprint --check-sigs %r"

# read in the public key ring
set pgp_list_pubring_command="gpg --no-verbose --batch --quiet --with-colons --list-keys %r" 

# read in the secret key ring
set pgp_list_secring_command="gpg --no-verbose --batch --quiet --with-colons --list-secret-keys %r" 

# fetch keys
# set pgp_getkeys_command="pkspxycwrap %r"
# This will work when #172960 will be fixed upstream
# set pgp_getkeys_command="gpg --recv-keys %r"

# pattern for good signature - may need to be adapted to locale!

# set pgp_good_sign="^gpgv?: Good signature from "

# OK, here's a version which uses gnupg's message catalog:
# set pgp_good_sign="`gettext -d gnupg -s 'Good signature from "' | tr -d '"'`"

# This version uses --status-fd messages
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"


set sendmail = "nullmailer-inject"
set envelope_from = "yes"

auto_view text/html                                      # view html automatically
alternative_order text/plain text/enriched text/html     # save html for last

# Ignore all headers
ignore *

# Then un-ignore the ones I want to see
unignore From:
unignore To:
unignore Reply-To:
unignore Subject:
unignore Date:
unignore Organization:
#unignore CC:
#unignore BCC:
unignore X-Mailer:
unignore User-Agent:
#unignore X-Junked-Because:
#unignore X-SpamProbe:
#unignore X-Virus-hagbard:
unignore X-Clacks-Overhead:

my_hdr X-Clacks-Overhead: GNU Terry Pratchett, GNU Professor Stephen Hawking

# Now order the visable header lines
#hdr_order From: Subject: To: CC: BCC: Reply-To: Mail-Followup-To: Date: Organization: User-Agent: X-Mailer:
#

source ~/.mutt/dracula.muttrc

# notmuch

  macro index <F8> \
  "<enter-command>unset wait_key<enter><shell-escape>notmuch-mutt --prompt search<enter><change-folder-readonly>`echo ${XDG_CACHE_HOME:-$HOME/.cache}/notmuch/mutt/results`<enter>" \
  "notmuch: search mail"
  macro index <F9> \
  "<enter-command>unset wait_key<enter><pipe-message>notmuch-mutt thread<enter><change-folder-readonly>`echo ${XDG_CACHE_HOME:-$HOME/.cache}/notmuch/mutt/results`<enter><enter-command>set wait_key<enter>" \
  "notmuch: reconstruct thread"
  macro index <F6> \
  "<enter-command>unset wait_key<enter><pipe-message>notmuch-mutt tag -inbox<enter>" \
  "notmuch: remove message from inbox"
