#!/bin/bash

root="$(pwd)"

mkdir -p ~/.i3
mkdir -p ~/Maildir/fuse.pl/
mkdir -p ~/.config/nvim

ln -sf "$root/Dots/i3config" ~/.i3/config
ln -sf "$root/Dots/i3status.conf" ~/.i3status.conf
ln -sf "$root/Dots/mailcap" ~/.mailcap
ln -sf "$root/Dots/mailcheckrc" ~/.mailcheckrc
ln -sf "$root/Dots/muttrc" ~/.muttrc
ln -sf "$root/Dots/offlineimaprc" ~/.offlineimaprc
ln -sf "$root/Dots/tmux.conf" ~/.tmux.conf
ln -sf "$root/Dots/vimrc" ~/.vimrc
ln -sf "$root/Dots/msmtprc" ~/.msmtprc
ln -sf "$root/Fonts" ~/.fonts
ln -sf "$root/Wallpapers" ~/Wallpapers
ln -sf "$root/Dots/rainbow_config.json" ~/.rainbow_config.json
ln -sf "$root/Dots/Notmuch" ~/.notmuch-config
ln -sf "$root/Dots/zsh" ~/.zshrc
ln -sf "$root/Dots/Profile" ~/.profile
ln -sf "$root/Dots/i3blocks" ~/.i3blocks.conf
ln -sf "$root/Dots/dracula.muttrc" ~/.mutt/dracula.muttrc
ln -sf ~/.vimrc ~/.config/nvim/init.vim
