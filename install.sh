#!/bin/bash

source ./packages.sh # get packages

# pre-req

apt update
DEBIAN_FRONTEND=noninteractive apt dist-upgrade -y
apt install curl dirmngr usbutils -y

cd /tmp
# Give me O, give me A, give me UNICODE I DESIRE

localedef -i pl_PL -f UTF-8 pl_PL.UTF-8

# Backports

#echo deb http://ftp.debian.org/debian strech-backports main | tee /etc/apt/sources.list.d/backports.list

# buster non-free, contrib

echo "deb http://ftp.ch.debian.org/debian/ buster contrib non-free" | tee /etc/apt/sources.list.d/nonfree.list

# Debian-multimedia

wget http://www.deb-multimedia.org/pool/main/d/deb-multimedia-keyring/deb-multimedia-keyring_2016.8.1_all.deb -O keyring.deb
sudo dpkg -i keyring.deb
echo "deb http://www.deb-multimedia.org buster main non-free" | tee /etc/apt/sources.list.d/multimedia.list


lscpu | grep x86_64 > /dev/null
if [ $? = 0 ]; then
	# Virtualbox

	echo deb http://download.virtualbox.org/virtualbox/debian bionic contrib | tee /etc/apt/sources.list.d/virtualbox.list
	wget https://www.virtualbox.org/download/oracle_vbox_2016.asc
	sudo apt-key add oracle_vbox_2016.asc

	apt update
	DEBIAN_FRONTEND=noninteractive apt dist-upgrade -y
	DEBIAN_FRONTEND=noninteractive apt install -y $packages

	# Vagrant

	wget https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.deb -O vagrant.deb
	dpkg -i vagrant.deb

	# BSD
	BOOTSTRAP_TAR="bootstrap-trunk-x86_64-20170127.tar.gz"
	BOOTSTRAP_SHA="eb0d6911489579ca893f67f8a528ecd02137d43a"
	curl -O https://pkgsrc.joyent.com/packages/Linux/el7/bootstrap/${BOOTSTRAP_TAR}
	sudo tar -zxpf ${BOOTSTRAP_TAR} -C /
	rm $BOOTSTRAP_TAR
fi

# Python
PATH="$HOME/.local/bin:$PATH"
pip3 install --user pipx 
pipx install youtube-dl 
pipx install tmuxp 
pipx install http-prompt 
pipx install goobook
pipx install mycli
pipx install pgcli
pipx install bpython

# Bluetooth subsystem

lsusb |grep Bluetooth -q
if [ $? = "0" ]; then
	# install BT related software
	DEBIAN_FRONTEND=noninteractive apt install -y $bluetooth_packages
fi

# oh my shell
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
