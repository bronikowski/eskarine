packages="i3 git vim mutt tmux arandr mplayer gimp \
	firefox-esr firefox-esr-l10n-pl build-essential python3-pip python3-dev xinit \
	network-manager-openvpn blueman imapfilter msmtp htop \
	network-manager sakura scrot mailcheck imagemagick \
	lightdm python-setuptools python3-setuptools qutebrowser \
	usbutils libjpeg-dev zlib1g-dev redshift \
	offlineimap vim-runtime curl mercurial \
	sudo rtmpdump libpango1.0-0 \
	dosbox scummvm libpq-dev \
	visualboyadvance libxslt1.1 tig \
	encfs pm-utils weechat zsh ntp \
	network-manager-gnome network-manager-openvpn-gnome \
	mc apvlv bluez-firmware unzip ranger \
	network-manager-gnome network-manager-openvpn-gnome \
	mc apvlv unzip ranger deluge cmake \
	pkg-config libtool libtool-bin automake libnotify-bin \
	dosfstools mtp-tools profile-sync-daemon vlock \
	ncdu exuberant-ctags jq sshfs filezilla \
	pcmanfm byzanz gifsicle feh tumbler-plugins-extra notmuch i3blocks \
	python3-venv aria2 nullmailer pmount
	"

bluetooth_packages="pavucontrol pulseaudio-module-bluetooth bluez-firmware obexftp obexpushd obex-data-server bluetooth"
